package com.whereta.dao.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.whereta.dao.ILoginLogDao;
import com.whereta.mapper.LoginLogMapper;
import com.whereta.model.LoginLog;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by vincent on 15-9-12.
 */
@Repository("loginLogDao")
public class LoginLogDaoImpl implements ILoginLogDao {
    @Resource
    private LoginLogMapper loginLogMapper;

    /**
     * 查询登录日志
     * @param pageNow
     * @param count
     * @return
     */
    @Override
    public PageInfo<LoginLog> query(int pageNow, int count) {
        PageHelper.startPage(pageNow,count);
        List<LoginLog> loginLogList = loginLogMapper.query();
        PageInfo<LoginLog> pageInfo=new PageInfo<>(loginLogList);
        return pageInfo;
    }
}
