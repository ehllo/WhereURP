package com.whereta.service.impl;

import com.github.pagehelper.PageInfo;
import com.whereta.dao.ILoginLogDao;
import com.whereta.model.LoginLog;
import com.whereta.service.ILogService;
import com.whereta.vo.ResultVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vincent on 15-9-12.
 */
@Service("logService")
public class LogServiceImpl implements ILogService {
    @Resource
    private ILoginLogDao loginLogDao;

    /**
     * 查询登录日志
     *
     * @param page
     * @param count
     * @return
     */
    @Override
    public ResultVO queryLoginLog(int page, int count) {
        ResultVO resultVO = new ResultVO(true);
        PageInfo<LoginLog> pageInfo = loginLogDao.query(page, count);
        Map<String, Object> dataMap = new HashMap<>();
        List<LoginLog> list = pageInfo.getList();
        for (LoginLog loginLog : list) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            loginLog.setShowLoginTime(format.format(loginLog.getLoginTime()));
        }
        dataMap.put("rows", list);
        dataMap.put("total", pageInfo.getTotal());
        resultVO.setData(dataMap);
        return resultVO;
    }

    /**
     * 获取所有用户分布地址坐标
     *
     * @return
     */
    public ResultVO getAllUserLocations() {

        ResultVO resultVO = new ResultVO(true);

        PageInfo<LoginLog> pageInfo = loginLogDao.query(1, Integer.MAX_VALUE);

        List<LoginLog> list = pageInfo.getList();

        List<Map<String, Double>> mapList = new ArrayList<>();

        for (LoginLog loginLog : list) {
            BigDecimal pointX = loginLog.getPointX();
            BigDecimal pointY = loginLog.getPointY();
            if (pointX != null && pointY != null) {
                Map<String, Double> map = new HashMap<>();
                map.put("x", pointX.doubleValue());
                map.put("y", pointY.doubleValue());
                mapList.add(map);
            }
        }
        resultVO.setData(mapList);
        return resultVO;

    }
}
